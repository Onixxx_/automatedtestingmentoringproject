package stepDefs;

import abstractClasses.page.AbstractPage;
import desktop.fragments.forms.FormField;
import desktop.page_factory_manager.PageFactoryManager;
import dto.Card;
import dto.DeliveryAddressInformation;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.Transpose;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static constants.DeliveryAddressFormConstants.*;
import static driver.SingletonDriver.getDriver;
import static driver.SingletonDriver.openPageByUrl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static utils.WebDriverWaiter.waitForPageReadyState;
import static utils.WebDriverWaiter.waitForVisibilityOfElement;

public class CheckoutStepDefinitions {
    PageFactoryManager pageFactory;
    AbstractPage abstractPage;

    @Before
    public void onInit() {
        pageFactory = new PageFactoryManager();
    }

    @After
    public void tearDown() {
        getDriver().close();
    }

    @Given("I am an anonymous customer with clear cookies")
    public void clearCookies() {
        getDriver().manage().deleteAllCookies();
    }

    @When("I open the {string} page")
    public void openPage(String page) {
        openPageByUrl(pageFactory.getPageByName(page).getPageUrl());
        waitForPageReadyState(4);
    }

    @And("I search for {string}")
    public void search(String query) {
        pageFactory.getHomePage().getHeader().searchForProducts(query);
    }

    @And("I am redirected to the {string} page")
    public void isRedirectedToPage(String page) throws ClassNotFoundException {
        assertTrue(pageFactory.getPageByName(page).checkUrl());
    }

    @And("Search results contain the following products")
    public void searchResultsContainTheFollowingProducts(List<String> productsNames) {
        assertTrue(pageFactory.getSearchPage().getFoundedProductsNames().containsAll(productsNames));
    }

    @And("I apply the following search filters")
    public void applyTheFollowingSearchFilters(Map<String, String> filters) {
        pageFactory.getSearchPage().getSearchFiltersForm().selectFilters(filters);
    }

    @Then("Search results contain only the following products")
    public void searchResultsContainOnlyTheFollowingProducts(List<String> productsNames) {
        List<String> differences = pageFactory.getSearchPage().getFoundedProductsNames().stream()
                .filter(element -> !productsNames.contains(element))
                .collect(Collectors.toList());
        assertEquals(0, differences.size());
    }

    @When("I click 'Add to basket' button for product with name {string}")
    public void clickAddToBasketButtonForProductWithName(String productName) {
        pageFactory.getSearchPage().clickAddProductToBasket(productName);
    }

    @And("I select 'Basket\\Checkout' in basket pop-up")
    public void selectBasketCheckoutInBasketPopUp() {
        waitForVisibilityOfElement(4, pageFactory.getSearchPage().getBasketPopup().getRootElement());
        pageFactory.getSearchPage().getBasketPopup().proceedToBasketCheckout();
    }

    @And("Basket order summary is as following:")
    public void basketOrderSummaryIsAsFollowing(@Transpose Map<String, String> expectedTotals) {
        assertTrue(pageFactory.getBasketPage().getOrderSummary().isOrderSummaryAsFollowing(expectedTotals));
    }

    @When("I click 'Checkout' button on 'Basket' page")
    public void clickCheckoutButtonOnBasketPage() {
        pageFactory.getBasketPage().proceedToCheckoutPayment();
    }

    @When("I click 'Buy now' button")
    public void clickBuyNowButton() {
        pageFactory.getCheckoutPage().clickBuyNowButton();
    }

    @Then("the following validation error messages are displayed on 'Delivery Address' form:")
    public void validationErrorMessagesAreDisplayedOnDeliveryAddressForm(Map<String, String> expectedErrorMessages) {
        for (String element : expectedErrorMessages.keySet()) {
            assertEquals(expectedErrorMessages.get(element),
                    pageFactory.getCheckoutPage().getDeliveryAddressForm().getErrorMessageOfInput(element));
        }
    }

    @And("the following validation error messages are displayed on 'Payment' form:")
    public void theFollowingValidationErrorMessagesAreDisplayedOnPaymentForm(String expectedMessages) {
        List<String> expectedMessagesList = new ArrayList<>(Arrays.asList(expectedMessages.split(", ")));
        assertEquals(expectedMessagesList, pageFactory.getCheckoutPage().getPaymentForm().getGlobalErrorMessages());
    }

    @And("Checkout order summary is as following:")
    public void checkoutOrderSummaryIsAsFollowing(@Transpose Map<String, String> expectedTotals) {
        assertTrue(pageFactory.getCheckoutPage().getOrderSummary().isOrderSummaryAsFollowing(expectedTotals));
    }

    @And("I checkout as a new customer with email {string}")
    public void checkoutAsANewCustomerWithEmail(String email) {
        pageFactory.getCheckoutPage().getDeliveryAddressForm().provideEmail(email);
    }

    @When("I fill delivery address information manually:")
    public void fillDeliveryAddressInformationManually(DeliveryAddressInformation data) {
        pageFactory.getCheckoutPage().getDeliveryAddressForm().fillDeliveryAddressForm(data);
    }

    @Then("there is no validation error messages displayed on 'Delivery Address' form")
    public void thereIsNoValidationErrorMessagesDisplayedOnDeliveryAddressForm() {
        Map<String, FormField> errorMessages = pageFactory.getCheckoutPage().getDeliveryAddressForm().getFormFields();
        for (String key : errorMessages.keySet()) {
            if (errorMessages.get(key).getFieldErrorMessageLocator() != null) {
                assertTrue(errorMessages.get(key).getFieldErrorMessageLocator().getText().isEmpty());
            }
        }
    }

    @When("I enter my card details")
    public void enterCardDetails(Map<String, String> entry) {
        Card card = new Card(entry.get("cardNumber"), entry.get("Expiry Year"),
                entry.get("Expiry Month"), entry.get("Cvv"));
        pageFactory.getCheckoutPage().getPaymentForm().fillPaymentForm(card);
    }

    @DataTableType
    public DeliveryAddressInformation deliveryAddressInformationEntry(Map<String, String> entry) {
        return new DeliveryAddressInformation(entry.get(FULL_NAME), entry.get(DELIVERY_COUNTRY),
                entry.get(ADDRESS_LINE_1), entry.get(ADDRESS_LINE_2), entry.get(CITY), entry.get(COUNTY),
                entry.get(POSTCODE));
    }
}
