package utils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class KeyboardUtils {

    public static void submit(WebElement element) {
        element.sendKeys(Keys.ENTER);
    }

    public static void moveOutOfTheField(WebElement field) {
        field.sendKeys(Keys.TAB);
    }
}
