package utils;

import desktop.fragments.forms.FormField;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public interface HasFormFields {

    Map<String, FormField> getFormFields();

    default void setInputValue(String inputFieldName, String value) {
        WebElement input = getFormFields().get(inputFieldName).getFieldLocator();
        input.sendKeys(value);
    }

    default String getErrorMessageOfInput(String inputFieldName) {
        return getFormFields().get(inputFieldName).getFieldErrorMessageLocator().getText();
    }

    default void setOptionByVisibleText(String selectFieldName, String option) {
        Select select = new Select(getFormFields().get(selectFieldName).getFieldLocator());
        select.selectByVisibleText(option);
    }
}
