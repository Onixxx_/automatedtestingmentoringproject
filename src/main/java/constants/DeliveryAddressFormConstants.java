package constants;

public class DeliveryAddressFormConstants {

    public static final String EMAIL_ADDRESS = "Email address";
    public static final String FULL_NAME = "Full name";
    public static final String DELIVERY_COUNTRY = "Delivery country";
    public static final String ADDRESS_LINE_1 = "Address line 1";
    public static final String ADDRESS_LINE_2 = "Address line 2";
    public static final String CITY = "Town/City";
    public static final String COUNTY = "County/State";
    public static final String POSTCODE = "Postcode/ZIP";

    private DeliveryAddressFormConstants() {
    }
}