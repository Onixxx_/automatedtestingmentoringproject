package constants;

public class FilterConstants {

    public static final String PRICE = "Price range";
    public static final String AVAILABILITY = "Availability";
    public static final String LANG = "Language";
    public static final String FORMAT = "Format";

    private FilterConstants() {
    }
}