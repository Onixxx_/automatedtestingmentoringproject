package desktop.fragments.forms;

import org.openqa.selenium.WebElement;

public class FormField {

    private final WebElement fieldLocator;
    private WebElement fieldErrorMessageLocator;

    public FormField(WebElement fieldLocator) {
        this.fieldLocator = fieldLocator;
    }

    public FormField(WebElement fieldLocator, WebElement fieldErrorMessageLocator) {
        this.fieldLocator = fieldLocator;
        this.fieldErrorMessageLocator = fieldErrorMessageLocator;
    }

    public WebElement getFieldLocator() {
        return fieldLocator;
    }

    public WebElement getFieldErrorMessageLocator() {
        return fieldErrorMessageLocator;
    }

}
