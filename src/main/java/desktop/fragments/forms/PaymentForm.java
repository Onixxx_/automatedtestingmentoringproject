package desktop.fragments.forms;

import abstractClasses.fragment.AbstractFragment;
import dto.Card;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.HasFormFields;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static driver.SingletonDriver.getDriver;

public class PaymentForm extends AbstractFragment implements HasFormFields {

    @FindBy(css = "div.card-form")
    private WebElement rootElement;
    @FindBy(css = "input#credit-card-number")
    private WebElement cardNumber;
    @FindBy(css = "input#expiration")
    private WebElement expiryDate;
    @FindBy(css = "input#cvv")
    private WebElement cvv;

    public PaymentForm() {
        setRootElement(rootElement);
    }

    @Override
    public Map<String, FormField> getFormFields() {
        Map<String, FormField> formFields = new HashMap<>();
        formFields.put("cardNumber", new FormField(cardNumber));
        formFields.put("expiryDate", new FormField(expiryDate));
        formFields.put("Cvv", new FormField(cvv));

        return formFields;
    }

    public List<String> getGlobalErrorMessages() {
        return Arrays.asList(getChildElement(getErrorMessages()).getText().split("\n"));
    }

    public void fillPaymentForm(Card cardInfo) {
        getDriver().switchTo().frame("braintree-hosted-field-number");
        setInputValue("cardNumber", cardInfo.getCardNumber());

        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("braintree-hosted-field-expirationDate");
        setInputValue("expiryDate", cardInfo.getExpiryMonth() + cardInfo.getExpiryYear());

        getDriver().switchTo().defaultContent();
        getDriver().switchTo().frame("braintree-hosted-field-cvv");
        setInputValue("Cvv", cardInfo.getCvv());

        getDriver().switchTo().defaultContent();
    }

    private By getErrorMessages() {
        return By.cssSelector("div.buynow-error-msg");
    }
}
