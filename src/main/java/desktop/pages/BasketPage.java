package desktop.pages;

import abstractClasses.page.AbstractPage;
import desktop.fragments.order_summary.BasketOrderSummary;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utils.JavaScriptExecutorActions.clickButtonUsingJS;

public class BasketPage extends AbstractPage {

    @FindBy(css = "div.checkout-btns-wrap a.optimizely-variation-1")
    private WebElement checkoutButton;

    private final String URL = "https://www.bookdepository.com/basket";

    public BasketPage() {
        super();
        setPageUrl(URL);
    }

    public void proceedToCheckoutPayment() {
        clickButtonUsingJS(checkoutButton);
    }

    public BasketOrderSummary getOrderSummary() {
        return new BasketOrderSummary();
    }
}
