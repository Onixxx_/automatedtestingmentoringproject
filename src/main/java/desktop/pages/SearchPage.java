package desktop.pages;

import abstractClasses.page.AbstractPage;
import desktop.fragments.BasketPopup;
import desktop.fragments.forms.SearchFiltersForm;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class SearchPage extends AbstractPage {

    private final String URL = "https://www.bookdepository.com/search?searchTerm=";

    private final String URL_PATTERN = "[https://www.bookdepository.com/search?searchTerm=].+";

    @FindBy(css = "div.book-item h3.title a")
    private List<WebElement> foundedProducts;

    @FindBy(css = "a.add-to-basket")
    private List<WebElement> addToBasketButton;

    public SearchPage() {
        super();
        setPageUrl(URL);
        setPageUrlPattern(URL_PATTERN);
    }

    public List<String> getFoundedProductsNames() {
        return foundedProducts.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public SearchFiltersForm getSearchFiltersForm() {
        return new SearchFiltersForm();
    }

    public BasketPopup getBasketPopup() {
        return new BasketPopup();
    }

    public void clickAddProductToBasket(String productName) {
        int index = getFoundedProductsNames().indexOf(productName);
        addToBasketButton.get(index).click();
    }
}